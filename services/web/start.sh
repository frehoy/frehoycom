#!/bin/bash

if [ ! -f /etc/nginx/ssl/dhparam.pem ]; then
    echo "Creating dhparams"
    openssl dhparam -out /etc/letsencrypt/live/frehoy.com/dhparam.pem 1024
fi

echo "Starting nginx…"
nginx -g 'daemon off;'