docker run \
    -p 80:80 \
    --mount='type=volume,src=letsencrypt,dst=/etc/letsencrypt' \
    certbot/certbot certonly \
        --standalone \
        --register-unsafely-without-email \
        --agree-tos \
        --non-interactive \
        -d frehoy.com \
